// Realiza mudança no FontSize dos botões para evitar quebra no layout
function resizeButtons(){

    let minCaractersForResize = 11;
    let fontSizeResize = "11px";

    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;

    let buttons = document.getElementsByClassName("btn");

    if(windowWidth>765){
        for(let i=0; i<buttons.length; i++){
            if(buttons[i].innerHTML.length > minCaractersForResize){
                buttons[i].style.fontSize = fontSizeResize;
            }
        }
    }else{
        for(let i=0; i<buttons.length; i++){
            if(buttons[i].innerHTML.length > minCaractersForResize){
                buttons[i].style.fontSize = fontSizeResize;
            }
        }
    }

  };